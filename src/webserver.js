'use strict'
const express = require('express')
const path = require('path')
const fs = require('fs').promises

const app = express()

app.use((req, res, next) => {
    res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate')
    next()
})

const file = path.join(__dirname, 'messages.json')

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use('/', express.static('public'))
app.use(express.json())
app.use(express.urlencoded({extended: true}))

const emojiMapping = {
    ':grinning:': '😀',
    ':blush:': '😊',
    ':thumbsup:': '👍',
    ':heart:': '❤️',
    ':fire:': '🔥',
    ':rocket:': '🚀',
    ':eyes:': '👀',
}

function replaceEmojis(text) {
    for (const [key, value] of Object.entries(emojiMapping)) {
        text = text.replace(new RegExp(key, 'g'), value)
    }
    return text
}

async function readMessages() {
    try {
        const data = await fs.readFile(file, 'utf8')
        return JSON.parse(data).messages
    } catch (err) {
        console.error('Erreur lors de la lecture des messages :', err)
        throw err
    }
}

async function writeMessages(messages) {
    try {
        await fs.writeFile(file, JSON.stringify({messages}, null, 2), 'utf8')
        return messages
    } catch (err) {
        console.error('Erreur lors de l’écriture des messages :', err)
        throw err
    }
}

async function getNextMessageId() {
    const messages = await readMessages()
    const lastLoadedId = messages.reduce((maxId, message) => Math.max(maxId, message.id), 0)
    return lastLoadedId + 1
}

app.get('/', async(req, res) => {
    try {
        const messages = await readMessages()
        res.render('index', {title: 'Ma Page de Messages', messages})
    } catch (err) {
        console.error(err)
        res.status(500).json({error: 'Erreur lors de la récupération des messages'})
    }
})

app.post('/messages', async(req, res) => {
    const {sender, content} = req.body
    if (!sender || !content) {
        return res.status(400).json({error: 'Sender et content sont requis'})
    }

    try {
        const nextMessageId = await getNextMessageId()
        const newMessage = {id: nextMessageId, sender, content: replaceEmojis(content), timestamp: new Date().toISOString()}
        const messages = await readMessages()
        messages.push(newMessage)
        await writeMessages(messages)
        res.redirect('/')
    } catch (err) {
        console.error(err)
        res.status(500).json({error: 'Erreur lors de l’enregistrement du message'})
    }
})

app.delete('/messages/:id', async(req, res) => {
    const messageId = parseInt(req.params.id)

    if (isNaN(messageId)) {
        return res.status(400).json({error: 'ID invalide'})
    }

    try {
        const messagesBeforeDeletion = await readMessages()

        const messageIndex = messagesBeforeDeletion.findIndex(message => message.id === messageId)

        if (messageIndex === -1) {
            return res.status(404).json({error: 'Message non trouvé'})
        }

        messagesBeforeDeletion.splice(messageIndex, 1)

        const messagesAfterDeletion = await writeMessages(messagesBeforeDeletion)

        console.log('Messages après suppression :', messagesAfterDeletion)

        res.status(200).json({message: 'Message supprimé avec succès'})
    } catch (err) {
        console.error(err)
        res.status(500).json({error: 'Erreur lors de la suppression du message'})
    }
})

app.delete('/messages/sender/:senderName', async(req, res) => {
    const senderName = req.params.senderName
    try {
        const messages = await readMessages()
        const filteredMessages = messages.filter(message => message.sender !== senderName)
        await writeMessages(filteredMessages)
        res.json({message: `Tous les messages de ${senderName} ont été supprimés avec succès.`})
    } catch (err) {
        console.error(err)
        res.status(500).json({error: 'Erreur lors de la suppression des messages'})
    }
})

module.exports = app

