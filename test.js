'use strict'
const request = require('supertest')
const app = require('./src/webserver.js')

describe('WEB server', () => {
    let server

    beforeAll((done) => {
        server = app.listen(4000, done)
    })

    afterAll(() => {
        return new Promise((resolve) => server.close(resolve))
    })

    test('GET / should return the index page with messages', async() => {
        const res = await request(server).get('/')
        expect(res.statusCode).toEqual(200)
        expect(res.text).toContain('Ma Page de Messages')
    })

    test('POST /messages should create a new message', async() => {
        const res = await request(server)
            .post('/messages')
            .send({sender: 'Test Sender', content: 'Hello World'})
            .set('Accept', 'application/json')

        expect(res.statusCode).toEqual(302)
    })

})

